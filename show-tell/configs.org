* What is this?
  A collection of public Emacs configurations to get inspired and see what other people have configured.
* From the group
** [[https://gitlab.com/mgttlinger/dotfiles/tree/master/.emacs.d][mgttlinger]]                                                     :helm:mu4e:
* External
** [[https://github.com/dakra/dmacs/blob/master/init.org][dakra/dmacs]]
** [[https://github.com/Atman50/emacs-config][Atman50]]
** [[https://github.com/jacmoe/emacs.d][jacmoe]]
** [[https://github.com/patrickt/emacs][patrickt]]
